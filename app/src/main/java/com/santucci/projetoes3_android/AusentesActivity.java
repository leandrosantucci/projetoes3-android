package com.santucci.projetoes3_android;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class AusentesActivity extends AppCompatActivity {

    ArrayList<Veiculo> veiculos = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ausentes);
        ListView listaAusentes = (ListView) findViewById(R.id.listaAusentes);
        veiculos = Veiculo.SelectAllAusentes(this);
        ItemCarList lista_ausentes = new ItemCarList(this,veiculos,false);
        listaAusentes = (ListView) findViewById(R.id.listaAusentes);
        listaAusentes.setAdapter(lista_ausentes);

        final ListView finalListaAusentes = listaAusentes;
        listaAusentes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                System.out.println("Entrou");
                ListAdapter temp;
                temp = finalListaAusentes.getAdapter();
                System.out.println("Veiculo Selecionado: "+ ((Veiculo)temp.getItem(position)).getCodigo());
            }
        });
    }
}
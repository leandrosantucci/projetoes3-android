package com.santucci.projetoes3_android;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;

import static java.lang.Thread.sleep;

public class Settings extends AppCompatActivity implements Runnable{
    Thread processo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
    }

    public void btnAtualizarDBonClick(View v){
        if(processo == null){
            processo = new Thread(this, "atualizandoDB");
            processo.start();
            TextView txtMessage = (TextView) findViewById(R.id.tvMessage);
            txtMessage.setVisibility(View.VISIBLE);
            txtMessage.setText("CONECTANDO AO DB EXTERNO...");
        }
        //thread.start();
    }

    @Override
    public void run() {
        BancoDeDados db = new BancoDeDados("jdbc:jtds:sqlserver://10.0.2.2:1433","BancoPark");

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView txtMessage = (TextView) findViewById(R.id.tvMessage);
                txtMessage.setText("ATUALIZANDO MOTORISTAS...");
            }
        });

        DbAndroid dbAndroid = new DbAndroid(this);
        dbAndroid.deletarTabela("Motorista_Veiculo");
        dbAndroid.deletarTabela("Motorista");
        dbAndroid.deletarTabela("Veiculo");
        dbAndroid.criarTabelas();

        ArrayList<String[]> str = db.selection("SELECT * FROM Motorista");
        for(String[] dado: str){
            if(dbAndroid.inserir("INSERT INTO Motorista VALUES('"+dado[0]+"','"+dado[1]+"','"+dado[2]+"','"+dado[3]+"','"+dado[4]+"','"+dado[5]+"');") == false){
                processo = null;
                break;
            }
        }

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView txtMessage = (TextView) findViewById(R.id.tvMessage);
                txtMessage.setText("ATUALIZANDO VEICULOS...");
            }
        });
        str.clear();
        str = db.selection("SELECT * FROM Veiculo");
        for(String[] dado: str){
            if(dbAndroid.inserir("INSERT INTO Veiculo(VeiCod,VeiPlaca,VeiMarca,VeiModelo,VeiCor,VeiAdesivo) VALUES('"+Integer.parseInt(dado[0])+"','"+dado[1]+"','"+dado[2]+"','"+dado[3]+"','"+dado[4]+"','"+dado[5]+"');") == false){
                processo = null;
                break;
            }
        }

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView txtMessage = (TextView) findViewById(R.id.tvMessage);
                txtMessage.setText("ATUALIZANDO ASSOCIAÇÕES...");
            }
        });

        str.clear();
        str = db.selection("SELECT * FROM Motorista_Veiculo");

        for(String[] dado: str){
            if(dbAndroid.inserir("INSERT INTO Motorista_Veiculo VALUES('"+dado[0]+"','"+Integer.parseInt(dado[1])+"');") == false){
                processo = null;
                break;
            }
        }

        db.fecharConex();

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView txtMessage = (TextView) findViewById(R.id.tvMessage);
                txtMessage.setText("ATUALIZADO COM SUCESSO!");
            }
        });

        try {
            sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView txtMessage = (TextView) findViewById(R.id.tvMessage);
                txtMessage.setVisibility(View.INVISIBLE);
            }
        });
        processo = null;
    }
}

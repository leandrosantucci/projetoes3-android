package com.santucci.projetoes3_android;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Transition;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class ItemSelecionado extends AppCompatActivity {

    private Veiculo veiculo;

    private ArrayList<Motorista> motoristas;
    /*public ItemSelecionado(Veiculo veiculo){
        this.veiculo = veiculo;
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_selecionado);

        veiculo = (Veiculo) getIntent().getExtras().getSerializable("veiculo");
        TextView txtPlaca = (TextView) findViewById(R.id.txtPlaca);
        TextView txtMarca = (TextView) findViewById(R.id.txtMarca);
        TextView txtModelo = (TextView) findViewById(R.id.txtModelo);
        TextView txtCor = (TextView) findViewById(R.id.txtCor);
        TextView txtAdesivo = (TextView) findViewById(R.id.txtAdesivo);

        txtPlaca.setText(veiculo.getPlaca());
        txtMarca.setText(veiculo.getMarca());
        txtModelo.setText(veiculo.getModelo());
        txtCor.setText(veiculo.getCor());
        txtAdesivo.setText(veiculo.getAdesivo());

        motoristas = veiculo.getMotoristas(this);
        ItemMotoList lista_motoristas = new ItemMotoList(this,motoristas);
        ListView listaMoto = (ListView) findViewById(R.id.listMotorista);
        listaMoto.setAdapter(lista_motoristas);
    }
}

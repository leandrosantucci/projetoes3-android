package com.santucci.projetoes3_android;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by santucci on 24/11/17.
 */

public class DbAndroid extends SQLiteOpenHelper {

    public DbAndroid(Context context) {
        super(context, "BancoPark", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String motorista = "CREATE TABLE Motorista" +
                "(MotRG VARCHAR(12) NOT NULL PRIMARY KEY," +
                "MotNomeCompleto VARCHAR(50) NOT NULL," +
                "MotSexo VARCHAR(9) NOT NULL," +
                "MotFoto TEXT NOT NULL," +
                "MotTelefone CHAR(15) NULL," +
                "MotTelefone2 CHAR(15) NULL" +
                ");";

        String veiculo = "CREATE TABLE Veiculo(" +
                "VeiCod NUMERIC(6) NOT NULL PRIMARY KEY," +
                "VeiPlaca VARCHAR(7) NOT NULL," +
                "VeiMarca VARCHAR(20) NOT NULL," +
                "VeiModelo VARCHAR(20) NOT NULL," +
                "VeiCor VARCHAR(20) NOT NULL," +
                "VeiAdesivo VARCHAR(6) NULL" +
                "VeiStatus VARCHAR(1) NULL DEFAULT 'A');";

        String motovei = "CREATE TABLE Motorista_Veiculo(" +
                "MotRG VARCHAR(12) NOT NULL REFERENCES Motorista," +
                "VeiCod NUMERIC(6) NOT NULL REFERENCES Veiculo," +
                "PRIMARY KEY(MotRG, VeiCod)" +
                ") ";
        try{
            sqLiteDatabase.execSQL(motorista);
            sqLiteDatabase.execSQL(veiculo);
            sqLiteDatabase.execSQL(motovei);
        }
        catch (SQLException e) {
            Log.e("Exception", "SQLException" + String.valueOf(e.getMessage()));
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void criarTabelas() {
        SQLiteDatabase banco = this.getWritableDatabase();

        String motorista = "CREATE TABLE Motorista" +
                "(MotRG VARCHAR(12) NOT NULL PRIMARY KEY," +
                "MotNomeCompleto VARCHAR(50) NOT NULL," +
                "MotSexo VARCHAR(9) NOT NULL," +
                "MotFoto TEXT NOT NULL," +
                "MotTelefone CHAR(15) NULL," +
                "MotTelefone2 CHAR(15) NULL" +
                ");";

        String veiculo = "CREATE TABLE Veiculo(" +
                "VeiCod NUMERIC(6) NOT NULL PRIMARY KEY," +
                "VeiPlaca VARCHAR(7) NOT NULL," +
                "VeiMarca VARCHAR(20) NOT NULL," +
                "VeiModelo VARCHAR(20) NOT NULL," +
                "VeiCor VARCHAR(20) NOT NULL," +
                "VeiAdesivo VARCHAR(6) NULL," +
                "VeiStatus VARCHAR(1) NULL DEFAULT 'A');";

        String motovei = "CREATE TABLE Motorista_Veiculo(" +
                "MotRG VARCHAR(12) NOT NULL REFERENCES Motorista," +
                "VeiCod NUMERIC(6) NOT NULL REFERENCES Veiculo," +
                "PRIMARY KEY(MotRG, VeiCod)" +
                ") ";
        try{
            banco.execSQL(motorista);
            banco.execSQL(veiculo);
            banco.execSQL(motovei);
        }
        catch (SQLException e) {
            Log.e("Exception", "SQLException" + String.valueOf(e.getMessage()));
            e.printStackTrace();
        }
    }

    public void deletarTabela(String tabela) {
        try{
            SQLiteDatabase bancodedados = this.getWritableDatabase();
            bancodedados.execSQL("DROP TABLE IF EXISTS " + tabela);
        }catch (SQLException e){
            Log.e("Exception", "SQLException" + String.valueOf(e.getMessage()));
            e.printStackTrace();
        }

    }

    public void update(String query) {
        try{
            SQLiteDatabase bancodedados = this.getWritableDatabase();
            bancodedados.execSQL(query);
        }catch (SQLException e){
            Log.e("Exception", "SQLException" + String.valueOf(e.getMessage()));
            e.printStackTrace();
        }

    }

    public boolean inserir(String query){
        SQLiteDatabase banco = this.getWritableDatabase();
        //Cursor dados = banco.rawQuery(query,null);
        try{
            banco.execSQL(query);
        }catch (SQLException e){
            Log.e("Exception", "SQLException" + String.valueOf(e.getMessage()));
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public ArrayList<String[]> selection(String query) {
        ArrayList<String[]> dados = new ArrayList();
        SQLiteDatabase banco = this.getReadableDatabase();
        Cursor cursor = banco.rawQuery(query,null);
        int maxColumn = cursor.getColumnCount();
        while (cursor.moveToNext()) {
            String[] dado = new String[maxColumn];
            for(int i = 0; i < maxColumn; i++){
                dado[i] = cursor.getString(i);
            }
            dados.add(dado);
        }
        cursor.close();
        return dados;
    }

}

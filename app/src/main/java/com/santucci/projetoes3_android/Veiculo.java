package com.santucci.projetoes3_android;
        import android.content.Context;

        import java.io.Serializable;
        import java.lang.reflect.Array;
        import java.util.ArrayList;

/**
 *
 * @author santucci
 */
public class Veiculo implements Serializable {

    public final int CONCLUIDO = 0;
    public final int ERROR_VALIDACAO = 1;
    public final int ERROR_BANCO = 2;

    private int codigo;
    private String placa;
    private String marca;
    private String modelo;
    private String cor;
    private String adesivo;
    private ArrayList<Motorista> motoristas = new ArrayList();

    public Veiculo(int codigo, String placa, String marca, String modelo, String cor, String adesivo) {
        this.codigo = codigo;
        this.placa = placa;
        this.marca = marca;
        this.modelo = modelo;
        this.cor = cor;
        if(!adesivo.equals("null")){
            this.adesivo = adesivo;
        }
    }

    public Veiculo() {

    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getAdesivo() {
        return adesivo;
    }

    public void setAdesivo(String adesivo) {
        this.adesivo = adesivo;
    }

    public boolean validarDados() {

        boolean saida = true;

        if (this.placa == null) {
            saida = false;
        } else if (this.placa.length() < 6) {
            saida = false;
        }

        if (this.cor == null) {
            saida = false;
        } else if (this.cor.length() < 2) {
            saida = false;
        }

        if (this.modelo == null) {
            saida = false;
        }

        if (this.marca == null) {
            saida = false;
        }
        return saida;
    }

    public ArrayList<Motorista> getMotoristas(Context context){
        DbAndroid db = new DbAndroid(context);
        motoristas.clear();
        ArrayList<String[]> dados = db.selection("Select * FROM Motorista INNER JOIN Motorista_Veiculo ON Motorista.MotRG = Motorista_Veiculo.MotRG WHERE Motorista_Veiculo.VeiCod = "+this.codigo);
        for (int i = 0; i < dados.size(); i++) {
            motoristas.add(new Motorista((((String[]) dados.get(i))[0]),
                    ((String[]) dados.get(i))[1],
                    ((String[]) dados.get(i))[2],
                    ((String[]) dados.get(i))[3],
                    ((String[]) dados.get(i))[4],
                    (((String[]) dados.get(i))[5])
            ));
        }
        db.close();
        return motoristas;
    }

    public static ArrayList<Veiculo> SelectAll(Context context) {
        ArrayList<Veiculo> veiculos = new ArrayList();
        DbAndroid db = new DbAndroid(context);
        ArrayList<String[]> dados = db.selection("Select * FROM Veiculo");

        for (int i = 0; i < dados.size(); i++) {
            veiculos.add(new Veiculo(Integer.parseInt(((String[]) dados.get(i))[0]),
                    ((String[]) dados.get(i))[1],
                    ((String[]) dados.get(i))[2],
                    ((String[]) dados.get(i))[3],
                    ((String[]) dados.get(i))[4],
                    (((String[]) dados.get(i))[5])
            ));
        }
        return veiculos;
    }


    public static ArrayList<Veiculo> SelectAllPresentes(Context context) {
        ArrayList<Veiculo> veiculos = new ArrayList();
        DbAndroid db = new DbAndroid(context);
        ArrayList<String[]> dados = db.selection("Select * FROM Veiculo WHERE VeiStatus = 'P'");

        for (int i = 0; i < dados.size(); i++) {
            veiculos.add(new Veiculo(Integer.parseInt(((String[]) dados.get(i))[0]),
                    ((String[]) dados.get(i))[1],
                    ((String[]) dados.get(i))[2],
                    ((String[]) dados.get(i))[3],
                    ((String[]) dados.get(i))[4],
                    (((String[]) dados.get(i))[5])
            ));
        }
        return veiculos;
    }


    public static ArrayList<Veiculo> SelectAllAusentes(Context context) {
        ArrayList<Veiculo> veiculos = new ArrayList();
        DbAndroid db = new DbAndroid(context);
        ArrayList<String[]> dados = db.selection("Select * FROM Veiculo WHERE VeiStatus = 'A' OR VeiStatus = NULL");

        for (int i = 0; i < dados.size(); i++) {
            veiculos.add(new Veiculo(Integer.parseInt(((String[]) dados.get(i))[0]),
                    ((String[]) dados.get(i))[1],
                    ((String[]) dados.get(i))[2],
                    ((String[]) dados.get(i))[3],
                    ((String[]) dados.get(i))[4],
                    (((String[]) dados.get(i))[5])
            ));
        }
        return veiculos;
    }

    public static ArrayList<Veiculo> SelectAllAssociado(String rg) {
        ArrayList<Veiculo> Veiculos = new ArrayList();
        BancoDeDados db = new BancoDeDados("jdbc:sqlserver://localhost:1433;", "BancoPark");
        ArrayList<String[]> dados = db.selection("SELECT Veiculo.VeiCod, "
                + "Veiculo.VeiPlaca, "
                + "Veiculo.VeiMarca, "
                + "Veiculo.VeiModelo, "
                + "Veiculo.VeiCor, "
                + "Veiculo.VeiAdesivo "
                + "FROM Motorista_Veiculo "
                + "INNER JOIN Veiculo ON Motorista_Veiculo.VeiCod = Veiculo.VeiCod "
                + "WHERE Motorista_Veiculo.MotRG = '" + rg + "'");

        for (int i = 0; i < dados.size(); i++) {
            Veiculos.add(new Veiculo(Integer.parseInt(((String[]) dados.get(i))[0]),
                    ((String[]) dados.get(i))[1],
                    ((String[]) dados.get(i))[2],
                    ((String[]) dados.get(i))[3],
                    ((String[]) dados.get(i))[4],
                    (((String[]) dados.get(i))[5])
            ));
        }
        return Veiculos;
    }

    public static ArrayList<Veiculo> SelectAllAssociadoExcept(String rg) {
        ArrayList<Veiculo> Veiculos = new ArrayList();
        BancoDeDados db = new BancoDeDados("jdbc:sqlserver://localhost:1433;", "BancoPark");
        System.out.println("RG do motorista que não deverá pegar a associação: " + rg);
        ArrayList<String[]> dados = db.selection("(SELECT Veiculo.VeiCod, Veiculo.VeiPlaca, Veiculo.VeiMarca, Veiculo.VeiModelo, Veiculo.VeiCor, Veiculo.VeiAdesivo  FROM Veiculo) EXCEPT (SELECT Veiculo.VeiCod, Veiculo.VeiPlaca, Veiculo.VeiMarca, Veiculo.VeiModelo, Veiculo.VeiCor, Veiculo.VeiAdesivo  FROM Motorista_Veiculo INNER JOIN Veiculo ON Motorista_Veiculo.VeiCod = Veiculo.VeiCod  WHERE Motorista_Veiculo.MotRG LIKE '" + rg + "')");

        for (int i = 0; i < dados.size(); i++) {
            Veiculos.add(new Veiculo(Integer.parseInt(((String[]) dados.get(i))[0]),
                    ((String[]) dados.get(i))[1],
                    ((String[]) dados.get(i))[2],
                    ((String[]) dados.get(i))[3],
                    ((String[]) dados.get(i))[4],
                    (((String[]) dados.get(i))[5])
            ));
        }
        System.out.println("Tamanho do Veiculos Não Pertencentes retornado: " + Veiculos.size());
        return Veiculos;
    }

    public static ArrayList<Veiculo> SelectAllAssociadoExcept(String rg, ArrayList<String> criterios) {
        ArrayList<Veiculo> Veiculos = new ArrayList();
        BancoDeDados db = new BancoDeDados("jdbc:sqlserver://localhost:1433;", "BancoPark");
        System.out.println("RG do motorista que não deverá pegar a associação: " + rg);
        String selection;
        selection = "(SELECT Veiculo.VeiCod, Veiculo.VeiPlaca, Veiculo.VeiMarca, Veiculo.VeiModelo, Veiculo.VeiCor, Veiculo.VeiAdesivo  FROM Veiculo WHERE ";
        for (String criterio : criterios) {
            selection += criterio + " AND ";
        }
        selection = selection.substring(0, selection.length() - 4);

        selection += ")EXCEPT (SELECT Veiculo.VeiCod, Veiculo.VeiPlaca, Veiculo.VeiMarca, Veiculo.VeiModelo, Veiculo.VeiCor, Veiculo.VeiAdesivo  FROM Motorista_Veiculo INNER JOIN Veiculo ON Motorista_Veiculo.VeiCod = Veiculo.VeiCod  WHERE Motorista_Veiculo.MotRG LIKE '" + rg + "')";

        System.out.println(selection);
        ArrayList<String[]> dados = db.selection(selection);

        for (int i = 0; i < dados.size(); i++) {
            Veiculos.add(new Veiculo(Integer.parseInt(((String[]) dados.get(i))[0]),
                    ((String[]) dados.get(i))[1],
                    ((String[]) dados.get(i))[2],
                    ((String[]) dados.get(i))[3],
                    ((String[]) dados.get(i))[4],
                    (((String[]) dados.get(i))[5])
            ));
        }
        System.out.println("Tamanho do Veiculos Não Pertencentes retornado: " + Veiculos.size());
        return Veiculos;
    }

    public static boolean RemoverPorCodigo(int codigo) {
        BancoDeDados db = new BancoDeDados("jdbc:sqlserver://localhost:1433;", "BancoPark");
        return db.delete("DELETE FROM Veiculo WHERE VeiCod = " + codigo);
    }

    public static ArrayList<Veiculo> SelectPorCriterio(ArrayList<String> criterios) {
        ArrayList<Veiculo> veiculos = new ArrayList();
        BancoDeDados db = new BancoDeDados("jdbc:sqlserver://localhost:1433;", "BancoPark");
        String select = "Select * FROM Veiculo WHERE ";

        for (String criterio : criterios) {
            select += criterio + " AND ";
        }
        select = select.substring(0, select.length() - 4);
        ArrayList<String[]> dados = db.selection(select);

        System.out.println(select);

        for (int i = 0; i < dados.size(); i++) {
            veiculos.add(new Veiculo(Integer.parseInt(((String[]) dados.get(i))[0]),
                    ((String[]) dados.get(i))[1],
                    ((String[]) dados.get(i))[2],
                    ((String[]) dados.get(i))[3],
                    ((String[]) dados.get(i))[4],
                    ((String[]) dados.get(i))[5]
            ));
        }
        return veiculos;
    }

    public int update() {
        if (validarDados()) {
            String updateQuery = "UPDATE Veiculo SET "
                    + " VeiPlaca = @VeiPlaca,"
                    + " VeiMarca = @VeiMarca, "
                    + " VeiModelo = @VeiModelo,"
                    + " VeiCor = @VeiCor,"
                    + " VeiAdesivo = @VeiAdesivo"
                    + " WHERE VeiCod = @VeiCodigo";
            updateQuery = updateQuery.replaceAll("@VeiCodigo", String.valueOf(this.codigo));
            updateQuery = updateQuery.replaceAll("@VeiPlaca", "'" + this.placa + "'");
            updateQuery = updateQuery.replaceAll("@VeiMarca", "'" + this.marca + "'");
            updateQuery = updateQuery.replaceAll("@VeiModelo", "'" + this.modelo + "'");
            updateQuery = updateQuery.replaceAll("@VeiCor", "'" + this.cor + "'");

            if (this.adesivo == null) {
                updateQuery = updateQuery.replaceAll("@VeiAdesivo", "NULL");
            } else {
                updateQuery = updateQuery.replaceAll("@VeiAdesivo", "'" + this.adesivo + "'");
            }

            BancoDeDados db = new BancoDeDados("jdbc:sqlserver://localhost:1433;", "BancoPark");
            if (!db.update(updateQuery)) {
                return ERROR_BANCO;
            }
        } else {
            return ERROR_VALIDACAO;
        }
        return CONCLUIDO;
    }

    @Override
    public String toString() {
        return "Veiculo{" + "codigo=" + codigo + ", placa=" + placa + ", marca=" + marca + ", modelo=" + modelo + ", cor=" + cor + ", adesivo=" + adesivo + '}';
    }
}

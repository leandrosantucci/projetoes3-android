package com.santucci.projetoes3_android;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;

public class ItemCarList extends ArrayAdapter<Veiculo> {

    private ArrayList<Veiculo> veiculos = new ArrayList();
    private final Activity context;
    private boolean presenca = false;

    public ItemCarList(Activity context, ArrayList<Veiculo> veiculos, boolean presenca) {
        super(context, R.layout.car_list_item, veiculos);
        // TODO Auto-generated constructor stub
        this.context = context;
        this.veiculos=veiculos;
        this.presenca=presenca;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.car_list_item, null,true);


        TextView txtPlaca = (TextView) rowView.findViewById(R.id.tvPlaca);
        TextView txtMarca = (TextView) rowView.findViewById(R.id.tvMarca);
        TextView txtModelo = (TextView) rowView.findViewById(R.id.tvModelo);
        TextView txtCor = (TextView) rowView.findViewById(R.id.tvCor);
        TextView txtAdesivo = (TextView) rowView.findViewById(R.id.tvAdesivo);
        Switch switchPresenca = (Switch) rowView.findViewById(R.id.switchP);

        txtPlaca.setText(veiculos.get(position).getPlaca());
        txtMarca.setText(veiculos.get(position).getMarca());
        txtModelo.setText(veiculos.get(position).getModelo());
        txtCor.setText(veiculos.get(position).getCor());
        if(veiculos.get(position).getAdesivo() == null){
            txtAdesivo.setText("Indefinido");
        }else{
            txtAdesivo.setText(veiculos.get(position).getAdesivo());
        }
        switchPresenca.setChecked(presenca);
        switchPresenca.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b == true){
                    DbAndroid db = new DbAndroid(context);
                    db.update("UPDATE Veiculo SET VeiStatus = 'P' WHERE VeiCod = "+veiculos.get(position).getCodigo());
                    System.out.println("ATIVO");
                    db.close();
                }else{
                    DbAndroid db = new DbAndroid(context);
                    db.update("UPDATE Veiculo SET VeiStatus = 'A' WHERE VeiCod = "+veiculos.get(position).getCodigo());
                    System.out.println("DESATIVADO");
                    db.close();

                }
            }
        });

        txtPlaca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println(veiculos.get(position).toString());
                Intent itemSelecView = new Intent(context,ItemSelecionado.class);
                //itemSelecView.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                itemSelecView.putExtra("veiculo",veiculos.get(position));
                context.startActivityForResult(itemSelecView,0);
            }
        });
        return rowView;
    };
}
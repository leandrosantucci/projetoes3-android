package com.santucci.projetoes3_android;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by santucci on 26/11/17.
 */

public class ItemMotoList extends ArrayAdapter<Motorista> {

    private final Activity context;
    private ArrayList<Motorista> motoristas;
    private boolean zoomOut =  false;

    public ItemMotoList(Activity context, ArrayList<Motorista> motoristas) {
        super(context, R.layout.moto_list_item, motoristas);
        // TODO Auto-generated constructor stub
        this.context = context;
        this.motoristas=motoristas;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.moto_list_item, null, true);

        TextView txtRG = (TextView) rowView.findViewById(R.id.txtRG);
        TextView txtNomeCompleto = (TextView) rowView.findViewById(R.id.txtNomeCompleto);
        TextView txtSexo = (TextView) rowView.findViewById(R.id.txtSexo);
        TextView txtTelefone = (TextView) rowView.findViewById(R.id.txtTel);
        TextView txtCelular = (TextView) rowView.findViewById(R.id.txtCel);
        final ImageView foto = (ImageView) rowView.findViewById(R.id.foto);

        txtRG.setText(motoristas.get(position).getRg());
        txtNomeCompleto.setText(motoristas.get(position).getNomeCompleto());
        if(motoristas.get(position).getSexo().equals("Feminino")){
            txtSexo.setText("F");
        }else{
            txtSexo.setText("M");
        }

        if(motoristas.get(position).getTelPrincipal() == null){
            txtTelefone.setText("Indefinido");
        }else{
            txtTelefone.setText(motoristas.get(position).getTelPrincipal());
        }

        if(motoristas.get(position).getTelRecados() == null){
            txtCelular.setText("Indefinido");
        }else{
            txtCelular.setText(motoristas.get(position).getTelRecados());

        }

        byte[] chartData = motoristas.get(position).getByteArrayImage();
        final Bitmap bm = BitmapFactory.decodeByteArray(chartData, 0, chartData.length);
        DisplayMetrics dm = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(dm);

        foto.setMinimumHeight(dm.heightPixels);
        foto.setMinimumWidth(dm.widthPixels);
        foto.setImageBitmap(bm);

        foto.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showImage(bm);
            }
        });

        return rowView;
    }

    public void showImage(Bitmap bm) {
        Dialog builder = new Dialog(context);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //nothing;
            }
        });

        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;

        ImageView imageView = new ImageView(context);
        imageView.setImageBitmap(Bitmap.createScaledBitmap(bm,width,width,false));
        builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        builder.show();
    }
}

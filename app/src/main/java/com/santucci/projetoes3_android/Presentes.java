package com.santucci.projetoes3_android;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class Presentes extends AppCompatActivity {

    ArrayList<Veiculo> veiculos = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_presentes);

        veiculos = Veiculo.SelectAllPresentes(this);
        ItemCarList lista_presentes = new ItemCarList(this,veiculos,true);
        ListView listaPresentes = (ListView) findViewById(R.id.listaP);
        listaPresentes.setAdapter(lista_presentes);
    }
}
